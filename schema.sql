-- Needed this because of the version of MySQL
-- ALTER USER root IDENTIFIED WITH mysql_native_password BY 'password';
CREATE TABLE IF NOT EXISTS notes
(
    id    INT AUTO_INCREMENT PRIMARY KEY,
    title TEXT NOT NULL
);

INSERT IGNORE INTO notes (id, title)
VALUES (100, 'test 1'),
       (101, 'test 2'),
       (102, 'test 3');