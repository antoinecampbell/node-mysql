const express = require("express");
const mysql = require('mysql');
const app = express();
// Enable the use of JSON for requests and responses
app.use(express.json());

// Create the database connection pool
const connectionPool = mysql.createPool({
  host: process.env['DB_HOST'] || 'localhost',
  user: process.env['DB_USERNAME'] || 'root',
  password: process.env['DB_PASSWORD'] || 'password',
  database: process.env['DB_DATABASE'] || 'notes'
});

// Fetch all notes
app.get("/notes", (req, res) => {
  // Get a connection from the connection pool
  connectionPool.getConnection((error, connection) => {
    // Use the connection to fetch all notes
    connection.query('SELECT * FROM notes', (err, rows) => {
      if (err) throw err;
      console.log('Notes:', rows);
      // Return all notes as json in the response
      res.json(rows);
    });
  });
});

// Create a new note
app.post('/notes', (req, res) => {
  // Get the note title from the body of the incoming request
  const title = req.body['title'];
  console.log('Title:', title);
  // Get a connection from the connection pool
  connectionPool.getConnection((error, connection) => {
    // Use the connection to insert a note
    connection.query('INSERT INTO notes (title) VALUES (?)', [title], (error, results, fields) => {
      // Return the title and the new note id as json in the response
      res.json({id: results.insertId, title})
    });
  });
});

// Delete a note by id
app.delete('/notes/:id', (req, res) => {
  // Get the note id from the url
  const noteId = req.params['id'];
  // Get a connection from the connection pool
  connectionPool.getConnection((error, connection) => {
    // Use the connection to delete a note by id
    connection.query('DELETE FROM notes WHERE id = ?', [noteId], (error, results, fields) => {
      // Return a message that the note was deleted
      res.json({message: `Note with id ${noteId} deleted`});
    });
  });
});

// Start the backend server
app.listen(3000, () => {
  console.log('Server is running at port 3000');
});

// Manually close the database connections when the app shuts down
function cleanup() {
  console.log('Shutting down the connection pool')
  connectionPool.end();
}

process.on('SIGINT', cleanup);
process.on('SIGTERM', cleanup);